# README #

Welcome to Telestream's public repository for Transform compositions. Please explore the [compositions](https://bitbucket.org/Telestream/transform/src/main/compositions/) folder. The [API templates](https://bitbucket.org/Telestream/transform/src/main/compositions/API%20Templates/) sub-folder contains compositions within full API request payload examples. For documentation, see: [https://docs.telestream.dev/docs](https://docs.telestream.dev/docs)

### Contribution guidelines ###

This repository provides read-only copies of compositons that are tested by QA. If you would like to provide or have a composition tested, please contact Spencer Williams (<spencerw@telestream.net>).

### Related links ###

* [Getting started](https://docs.telestream.dev/docs/getting-started-transform)
* [What are compositions?](https://docs.telestream.dev/docs/composition)
* [Telestream Cloud Transform](https://www.telestream.net/telestream-cloud/transform.htm)

